Configuration Documentation:

Each configuration is denoted by its name, followed by a colon, followed by its value.
The configuration options are title, x, y, width, height, red, green, blue, and alpha.
title is a string. X position, Y position, width, and height are integers. Values for color are floats (the f at the end is not needed).
The configuration options can be in any order. If a configuration option is not specified, it is set to a predefined default.