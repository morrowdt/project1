// ---------------------------------------------------------------------------
//  File name: main.cpp
//  Project name: Project 1
// ---------------------------------------------------------------------------
//  Creator�s name and email: Daniel Morrow, morrowdt@goldmail.etsu.edu		
//  Course-Section: 002
//  Creation Date: 8/31/2015			
// ---------------------------------------------------------------------------
#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

#include <Windows.h>
#include <iostream>
#include <fstream>

#include <GL\glew.h>
#include <GL\wglew.h>

#include <string>
using std::string;
using namespace std;

struct WindowStruct {
	HINSTANCE hInstance;
	HWND hWnd;
};

struct Context {
	HGLRC hrc;
	HDC hdc;
};

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
	case WM_CLOSE:
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

WindowStruct CreateCustomWindow(const string& title, int positionX, int positionY, int width, int height)
{
	WindowStruct window;
	window.hInstance = 0;
	window.hWnd = NULL;

	window.hInstance = GetModuleHandle(NULL);
	if (window.hInstance == 0) return window;

	WNDCLASS windowClass;
	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	windowClass.lpfnWndProc = (WNDPROC)WndProc;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = window.hInstance;
	windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.hbrBackground = NULL;
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = title.c_str();

	if (!RegisterClass(&windowClass)) {
		window.hInstance = 0;
		window.hWnd = NULL;
		return window;
	}

	window.hWnd = CreateWindowEx(
		WS_EX_APPWINDOW | WS_EX_WINDOWEDGE,
		title.c_str(),
		title.c_str(),
		WS_OVERLAPPEDWINDOW,
		//CW_USEDEFAULT, 0, 500, 500,
		positionX, positionY, width, height,
		NULL,
		NULL,
		window.hInstance,
		NULL
		);

	if (window.hWnd == NULL) {
		window.hInstance = 0;
		window.hWnd = NULL;
	}

	return window;
}

void MessageLoop(const Context& context)
{
	bool timeToExit = false;
	MSG msg;
	while (!timeToExit) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				timeToExit = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			glViewport(0, 0, 500, 500);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			SwapBuffers(context.hdc);
		}
	}
}

Context CreateOGLWindow(const WindowStruct& window)
{
	Context context;
	context.hdc = NULL;
	context.hrc = NULL;

	if (window.hWnd == NULL) return context;

	context.hdc = GetDC(window.hWnd);
	if (context.hdc == NULL) return context;

	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;

	int pixelFormat = ChoosePixelFormat(context.hdc, &pfd);
	if (pixelFormat == 0) {
		context.hdc = NULL;
		context.hrc = NULL;
		return context;
	}

	BOOL result = SetPixelFormat(context.hdc, pixelFormat, &pfd);
	if (!result) {
		context.hdc = NULL;
		context.hrc = NULL;
		return context;
	}

	HGLRC tempOpenGLContext = wglCreateContext(context.hdc);
	wglMakeCurrent(context.hdc, tempOpenGLContext);

	if (glewInit() != GLEW_OK) {
		context.hdc = NULL;
		context.hrc = NULL;
		return context;
	}

	return context;
}

void ShowWindow(HWND hWnd)
{
	if (hWnd != NULL) {
		ShowWindow(hWnd, SW_SHOW);
		UpdateWindow(hWnd);
	}
}

void CleanUp(const WindowStruct& window, const Context& context)
{
	if (context.hdc != NULL) {
		wglMakeCurrent(context.hdc, 0);
		wglDeleteContext(context.hrc);
		ReleaseDC(window.hWnd, context.hdc);
	}
}

string title = "WindowTitle";	// Default window title
int positionX = CW_USEDEFAULT;	// Default X position
int positionY = 0;				// Default Y position
int width = 500;				// Default width
int height = 500;				// Default height
float red = 0;					// Default red value
float green = 0;				// Default green value
float blue = 0;					// Default blue value
float alpha = 0;				// Default alpha value

/**
* Method Name: ReadConfig <br>
* Method Purpose: Reads the configuration for a windows from a file <br>
*
* <hr>
* Date created: 8/31/2015 <br>
* 
* <hr>
*   @param  configFile - name of the configuration file
*/

void ReadConfig(string configFile)
{
	string configData;		// storage for parsing config data
	ifstream fin;			// Input file stream
	fin.open(configFile);
	if (fin.fail()) 
	{
		cout << "\nThere was error opening the file." << endl;
		exit(1);
	}

	while (!fin.eof())		// While there is data to be read...
	{
		getline(fin, configData); // Read in a line from the configuration file
		if (configData.find("title:") != string::npos)			// If the line is a title, set the title
			title = configData.substr(6, string::npos);
		if (configData.find("x:") != string::npos)		// If the line is the position, convert and set the position
			positionX = stoi(configData.substr(2, string::npos));
		if (configData.find("y:") != string::npos)		// If the line is the position, convert and set the position
			positionY = stoi(configData.substr(2, string::npos));
		if (configData.find("width:") != string::npos)			// If the line is the width, convert and set the width
			width = stoi(configData.substr(6, string::npos));	
		if (configData.find("height:") != string::npos)			// If the line is the height, convert and set the height
			height = stoi(configData.substr(7, string::npos));
		if (configData.find("red:") != string::npos)			// If the line is the red value, convert and set the red value
			red = stof(configData.substr(4, string::npos));
		if (configData.find("green:") != string::npos)			// If the line is the green value, convert and set the green value
			green = stof(configData.substr(6, string::npos));
		if (configData.find("blue:") != string::npos)			// If the line is the blue value, convert and set the blue value
			blue = stof(configData.substr(5, string::npos));
		if (configData.find("alpha:") != string::npos)			// If the line is the alpha value, convert and set the alpha value
			alpha = stof(configData.substr(6, string::npos));
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	ReadConfig("window.config"); // Read config from windows.config
	WindowStruct window = CreateCustomWindow(title, positionX, positionY, width, height); // Create custom window with attributes
	if (window.hWnd != NULL) {
		Context context = CreateOGLWindow(window);
		if (context.hdc != NULL) {
			ShowWindow(window.hWnd);
			glClearColor(red, green, blue, alpha); // Set background color
			MessageLoop(context);
			CleanUp(window, context);
		}
	}

	return 0;
}